package rs.etf.km123247m.Utils;

import rs.etf.km123247m.GUI.GUIPolyListener;
import rs.etf.km123247m.Polynomials.Poly;

import java.util.ArrayList;

/**
 * User: Krca
 * Date: 6/27/13
 * Time: 8:32 PM
 */
public class PolyUpdater {

    private ArrayList<IPolyListener> qListeners = new ArrayList<IPolyListener>();
    private ArrayList<IPolyListener> rListeners = new ArrayList<IPolyListener>();
    private ArrayList<IPolyListener> hListeners = new ArrayList<IPolyListener>();
    private ArrayList<IPolyListener> iterationStartListeners = new ArrayList<IPolyListener>();
    private ArrayList<IPolyListener> iterationEndListeners = new ArrayList<IPolyListener>();
    private ArrayList<IPolyListener> calculationEndListeners = new ArrayList<IPolyListener>();

    public void QUpdate(Poly[] q, int i) {
        for(IPolyListener l: qListeners) {
            l.updateQ(q, i);
        }
    }

    public void RUpdate(Poly r) {
        for(IPolyListener l: rListeners) {
            l.updateR(r);
        }
    }

    public void HUpdate(Poly h) {
        for(IPolyListener l: hListeners) {
            l.updateH(h);
        }
    }

    public void iterationStart(int iteration) {
        for(IPolyListener l: iterationStartListeners) {
            l.updateIterationStart(iteration);
        }
    }

    public void iterationEnd(int iteration) {
        for(IPolyListener l: iterationEndListeners) {
            l.updateIterationEnd(iteration);
        }
    }

    public void calculationEnd(Poly[] q, Poly r) {
        for(IPolyListener l: calculationEndListeners) {
            l.updateCalculationEnd(q, r);
        }
    }

    public void registerQListener(IPolyListener listener) {
        qListeners.add(listener);
    }

    public void registerRListener(IPolyListener listener) {
        rListeners.add(listener);
    }

    public void registerHListener(IPolyListener listener) {
        hListeners.add(listener);
    }

    public void registerIterationStartListener(IPolyListener listener) {
        iterationStartListeners.add(listener);
    }

    public void registerIterationEndListener(IPolyListener listener) {
        iterationEndListeners.add(listener);
    }

    public void registerCalculationEndListener(IPolyListener listener) {
        calculationEndListeners.add(listener);
    }

    public void removeQListener(IPolyListener listener) {
        qListeners.remove(listener);
    }

    public void removeRListener(IPolyListener listener) {
        rListeners.remove(listener);
    }

    public void removeHListener(IPolyListener listener) {
        hListeners.remove(listener);
    }

    public void removeIterationStartListener(IPolyListener listener) {
        iterationStartListeners.remove(listener);
    }

    public void removeIterationEndListener(IPolyListener listener) {
        iterationEndListeners.remove(listener);
    }

    public void removeCalculationEndListener(IPolyListener listener) {
        calculationEndListeners.remove(listener);
    }

    public void removeAllListeners(GUIPolyListener listener) {
        this.removeHListener(listener);
        this.removeQListener(listener);
        this.removeRListener(listener);
        this.removeIterationStartListener(listener);
        this.removeIterationEndListener(listener);
        this.removeCalculationEndListener(listener);
    }

    public void registerAllListeners(GUIPolyListener listener) {
        this.registerHListener(listener);
        this.registerQListener(listener);
        this.registerRListener(listener);
        this.registerIterationStartListener(listener);
        this.registerIterationEndListener(listener);
        this.registerCalculationEndListener(listener);
    }
}
