package rs.etf.km123247m.Polynomials;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 10:39 AM
 */
public class PolyElement implements Cloneable {
    private ArrayList<PolyVariable> variables = null;
    private char sign = '+';

    public PolyElement(PolyVariable[] variables) {
        this.variables = new ArrayList<PolyVariable>();
        for (PolyVariable variable : variables) {
            this.variables.add(variable);
        }
    }

    public PolyElement(char sign, PolyVariable[] variables) {
        this.sign = sign;
        this.variables = new ArrayList<PolyVariable>();
        for (PolyVariable variable : variables) {
            this.variables.add(variable);
        }
    }

    public ArrayList<PolyVariable> getVariables() {
        return variables;
    }

    public void setVariables(ArrayList<PolyVariable> variables) {
        this.variables = variables;
    }

    public char getSign() {
        return sign;
    }

    public void setSign(char sign) {
        this.sign = sign;
    }

    @Override
    public String toString() {
        String s = sign + "";
        for (PolyVariable variable : this.variables) {
            s += variable.toString() + "*";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PolyElement)) return false;

        PolyElement that = (PolyElement) o;

        if (sign != that.sign) return false;
        if (variables != null ? !variables.equals(that.variables) : that.variables != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = variables != null ? variables.hashCode() : 0;
        result = 31 * result + (int) sign;
        return result;
    }

    public String getSymbols() {
        String symbols = "";
        ArrayList<Character> visitedSymbols = new ArrayList<Character>();
        for (Iterator iterator = this.variables.listIterator(); iterator.hasNext(); ) {
            PolyVariable variable = (PolyVariable) iterator.next();
            if (!Poly.isThereChar(visitedSymbols.toArray(new Character[]{}), variable.getVariable())
                    && !variable.isDigit()) {
                visitedSymbols.add(variable.getVariable());
            }
        }
        visitedSymbols = Poly.fixCharacterArray(visitedSymbols);
        for(Character c : visitedSymbols) {
            symbols += c + ", ";
        }
        if (symbols.length() > 2) {
            symbols = symbols.substring(0, symbols.length() - 2);
        }
        return symbols;
    }

    public PolyElement clone() {
        ArrayList<PolyVariable> variables = new ArrayList<PolyVariable>();
        for (Iterator iterator = this.getVariables().listIterator(); iterator.hasNext(); ) {
            variables.add(((PolyVariable) iterator.next()).clone());
        }
        PolyElement element = new PolyElement(variables.toArray(new PolyVariable[]{}));
        element.setVariables(variables);
        element.setSign(this.getSign());
        return element;
    }
}
