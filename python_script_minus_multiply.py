from __future__ import division
from sympy import *
from sympy import Poly

###############################################
############# Symbols #########################
###############################################

x, y, z = symbols('x y z')

###############################################
############# Functions #######################
###############################################

p = eval(poly1eval) + (eval(polyElemEval) * (-1) * eval(poly2eval))