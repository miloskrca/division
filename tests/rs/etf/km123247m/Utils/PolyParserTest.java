package rs.etf.km123247m.Utils;

import org.junit.Before;
import org.junit.Test;
import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Polynomials.PolyElement;
import rs.etf.km123247m.Polynomials.PolyVariable;

import static org.junit.Assert.assertEquals;

/**
 * User: Krca
 * Date: 6/22/13
 * Time: 1:35 PM
 */
public class PolyParserTest {
    Poly poly1;
    Poly poly2;

    @Before
    public void setUp() throws Exception {
//        y - z^3 - 1
        poly1 = new Poly(
                new PolyElement[]{
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('y')
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('z', 3)
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('1')
                                }
                        )
                }
        );

        //        -4 +y +z
        poly2 = new Poly(
                new PolyElement[]{
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('4')
                                }
                        ),
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('y')
                                }
                        ),
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('z')
                                }
                        )
                }
        );
    }

    @Test
    public void testParse() throws Exception {
        assertEquals(poly1, PolyParser.parse("+y -z**3 -1"));
        assertEquals(poly2, PolyParser.parse("-4 +y +z"));
    }
}
