package rs.etf.km123247m.GUI;

import javafx.fxml.FXML;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Utils.PolyParser;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * User: Krca
 * Date: 6/27/13
 * Time: 9:30 PM
 */
public class PolyController {

    @FXML
    private TextArea actiontarget;
    @FXML
    private Button loadFileButton;
    @FXML
    private Button startButton;
    @FXML
    private Button clearButton;
    @FXML
    private TextField chosenFile;

    private Stage stage;
    private GUIPolyListener listener;

    private Poly poly1;
    private ArrayList<Poly> polies;

    private boolean working;

    public PolyController() {
        this.polies = new ArrayList<Poly>();
    }

    @FXML
    protected void handleStartButtonAction(ActionEvent event) {
        if (poly1 == null) {
            actiontarget.appendText("\nIzaberite fajl...\n");
            return;
        }
        if (working == true) {
            return;
        }
        working = true;
        startButton.setDisable(true);
        clearButton.setDisable(true);
        loadFileButton.setDisable(true);
        actiontarget.appendText("\n============================================\n");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listener = new GUIPolyListener(actiontarget, poly1, polies.toArray(new Poly[polies.size()]));
                    poly1.getUpdater().registerAllListeners(listener);
                    poly1.divideWith(polies.toArray(new Poly[polies.size()]));
                    poly1.getUpdater().removeAllListeners(listener);
                    actiontarget.appendText("\n============================================\n");
                } catch (Exception e) {
                    e.printStackTrace();
                    actiontarget.appendText("\n\nDesila se nepoznata greska\n");
                    actiontarget.appendText("\n============================================\n");
                    poly1.getUpdater().removeAllListeners(listener);
                } finally {
                    startButton.setDisable(false);
                    clearButton.setDisable(false);
                    loadFileButton.setDisable(false);
                    working = false;
                }
            }
        }).start();
    }

    @FXML
    protected void loadFile(ActionEvent event) {
        actiontarget.appendText("\nUcitavanje...\n");
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File file = fileChooser.showOpenDialog(this.stage);
        if (file != null) {
            chosenFile.setText(file.getName());
            FileInputStream fis;
            BufferedReader br;
            try {
                String line;
                fis = new FileInputStream(file);
                br = new BufferedReader(new InputStreamReader(fis, Charset.forName("UTF-8")));
                line = br.readLine();
                StringBuffer buff = new StringBuffer();
                buff.append("(" + line.replace("**", "^") + ") / (");
                poly1 = PolyParser.parse(line);
                polies.clear();
                while ((line = br.readLine()) != null) {
                    // Deal with the line
                    buff.append(line.replace("**", "^") + ", ");
                    polies.add(PolyParser.parse(line));
                }
                buff.append(")\n");
                actiontarget.appendText(buff.toString().replace(", )", ")"));

                // Done with the file
                br.close();
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    protected void clearAction(ActionEvent event) {
        actiontarget.setText("");
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
