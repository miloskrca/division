package rs.etf.km123247m;

import org.junit.Before;
import org.junit.Test;
import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Utils.Divider;
import rs.etf.km123247m.Utils.PolyParser;

import static org.junit.Assert.assertEquals;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 8:53 PM
 */
public class ActualTest {
    Poly poly1;
    Poly poly2;
    Poly poly3;

    @Before
    public void setUp() throws Exception {
        poly1 = PolyParser.parse("+x**2*y +x*y**2 +y**2");
        poly2 = PolyParser.parse("+x*y -1");
        poly3 = PolyParser.parse("+y**2 -1");
    }

    @Test
    public void testDivideSingle() throws Exception {
        System.out.println("Testing DivideSingle... " + poly1 + " / " + poly2 + " ... ");
        Poly[] result = poly1.divideWith(poly2);
        assertEquals("+x +y", result[0].toString());
        assertEquals("+y**2 +x +y", result[1].toString());
    }

    @Test
    public void testDivideSingleNoSteps() throws Exception {
        System.out.println("Testing DivideSingleNoSteps... " + poly1 + " / " + poly2 + " ... ");
        Poly[] result = Divider.divideSingle(poly1, poly2);
        assertEquals("+x +y", result[0].toString());
        assertEquals("+x +y +y**2", result[1].toString());
    }

    @Test
    public void testDivideMultiple() throws Exception {
        System.out.println("Testing DivideMultiple... " + poly1 + " / (" + poly2 + ") (" + poly3 + ") ... ");
        Poly[][] result = poly1.divideWith(new Poly[]{poly2, poly3});
        System.out.println("DivideMultiple... " + result[0][0] + ", " + result[0][1] + " | " + result[1][0]);

        assertEquals("+x +y", result[0][0].toString());
        assertEquals("+1", result[0][1].toString());
        assertEquals("+x +y +1", result[1][0].toString());
    }

    // skipping because i don't know how to display entire q
//    @Test
//    public void testDivideMultipleNoSteps() throws Exception {
//        System.out.println("Testing DivideMultipleNoSteps... " + poly1 + " / (" + poly2 + ") (" + poly3 + ") ... ");
//        Poly[] result = Divider.divideMultiple(poly1, new Poly[]{poly2, poly3});
//        System.out.println("DivideMultipleNoSteps... " + result[0] + " | " + result[1]);
//
//        assertEquals("+x +y", result[0].toString());
//        assertEquals("+1 +x +y", result[1].toString());
//    }
}
