package rs.etf.km123247m.Polynomials;

import rs.etf.km123247m.Utils.Divider;
import rs.etf.km123247m.Utils.PolyParser;
import rs.etf.km123247m.Utils.PolyUpdater;

import java.util.ArrayList;
import java.util.Collections;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 10:27 AM
 */
public class Poly {
    private ArrayList<PolyElement> elements = null;
    private PolyUpdater updater = new PolyUpdater();

    public Poly(PolyElement[] elements) {
        this.elements = new ArrayList<PolyElement>();
        Collections.addAll(this.elements, elements);
    }

    public Poly() {
        this.elements = new ArrayList<PolyElement>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Poly)) return false;

        Poly poly = (Poly) o;

        return !(elements != null ? !elements.equals(poly.elements) : poly.elements != null);

    }

    @Override
    public int hashCode() {
        return elements != null ? elements.hashCode() : 0;
    }

    public ArrayList<PolyElement> getElements() {
        return elements;
    }

    public void setElements(ArrayList<PolyElement> elements) {
        this.elements = elements;
    }

    public void addElement(PolyElement element) {
        this.elements.add(element);
    }

    public Poly[] divideWith(Poly poly) {
        try {
            Poly h = this.clone();
            Poly q = new Poly();
            Poly r = new Poly();

            while (!(h.getElements().size() == 1 && h.getElements().get(0).toString().equals("+0"))) {
                if (h.isDividableBy(poly)) {
                    Poly firstQ = Divider.divideFirst(h, poly)[0];
                    q = Divider.add(q, firstQ);
                    h = Divider.sub(h, Divider.mul(firstQ, poly));
                } else {
                    r = Divider.add(r, PolyParser.parse(h.getElements().get(0).toString()));
                    h = Divider.sub(h, PolyParser.parse(h.getElements().get(0).toString()));
                }
            }

            return new Poly[]{q, r};
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return new Poly[]{};
        }
    }

    public Poly[][] divideWith(Poly[] polies) {
        try {
            Poly h = this.clone();

            Poly[] q = new Poly[polies.length];
            Poly r = new Poly();
            int iteration = 0;
            while (!(h.getElements().size() == 1 && h.getElements().get(0).toString().equals("+0"))) {
                updater.iterationStart(iteration);
                boolean divided = false;
                for (int i = 0; i < polies.length; i++) {
                    Poly poly = polies[i];
                    if (h.isDividableBy(poly)) {
                        if (q[i] == null) {
                            q[i] = new Poly();
                        }
                        Poly firstQ = Divider.divideFirst(h, poly)[0];
                        q[i] = Divider.add(q[i], firstQ);
                        updater.QUpdate(q, i);
                        h = Divider.sub(h, Divider.mul(firstQ, poly));
                        updater.HUpdate(h);
                        divided = true;
                        break;
                    }
                }
                if (!divided) {
                    r = Divider.add(r, PolyParser.parse(h.getElements().get(0).toString()));
                    updater.RUpdate(r);
                    h = Divider.sub(h, PolyParser.parse(h.getElements().get(0).toString()));
                    updater.HUpdate(h);
                }
                updater.iterationEnd(iteration++);
            }
            updater.calculationEnd(q, r);

            return new Poly[][]{q, new Poly[]{r}};

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isDividableBy(Poly poly) {
        return Divider.divides(this, poly);
    }

    @Override
    public String toString() {
        String s = "";
        for (PolyElement element : this.elements) {
            s += element.toString() + " ";
        }
        if (s.length() > 0) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    public String getSymbols() {
        String symbols = "";
        ArrayList<Character> visitedSymbols = new ArrayList<Character>();
        for (PolyElement element : this.elements) {
            PolyVariable[] vars = element.getVariables().toArray(new PolyVariable[element.getVariables().size()]);
            for (PolyVariable var : vars) {
                if (!Poly.isThereChar(visitedSymbols.toArray(new Character[visitedSymbols.size()]), var.getVariable())
                        && !var.isDigit()) {
                    visitedSymbols.add(var.getVariable());
                }
            }
        }
        visitedSymbols = Poly.fixCharacterArray(visitedSymbols);
        for (Character c : visitedSymbols) {
            symbols += c + ", ";
        }
        if (symbols.length() > 2) {
            symbols = symbols.substring(0, symbols.length() - 2);
        }
        return symbols;
    }

    public static Poly getZero() {
        return new Poly(new PolyElement[]{
                new PolyElement(new PolyVariable[]{
                        new PolyVariable('0')
                })
        });
    }

    @SuppressWarnings("CloneDoesntCallSuperClone")
    public Poly clone() throws CloneNotSupportedException {
        Poly poly = new Poly();
        ArrayList<PolyElement> elements = new ArrayList<PolyElement>();
        for (PolyElement polyElement : this.getElements()) {
            elements.add((polyElement).clone());
        }
        poly.setElements(elements);
        return poly;
    }

    public PolyUpdater getUpdater() {
        return updater;
    }

    public static boolean isThereChar(Character[] chaArray, Character chr) {
        boolean bool = false;
        for (Character aChaArray : chaArray) {
            if (chr.equals(aChaArray)) {
                bool = true;
            }
        }
        return bool;
    }

    public static ArrayList<Character> fixCharacterArray(ArrayList<Character> visitedSymbols) {
        ArrayList<Character> arr = new ArrayList<Character>();
        Character[] validChars = new Character[]{'x', 'y', 'z'};
        for (Character c : validChars) {
            if (isThereChar(visitedSymbols.toArray(new Character[visitedSymbols.size()]), c)) {
                arr.add(c);
            }
        }
        return arr;
    }
}
