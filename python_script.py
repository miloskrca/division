from __future__ import division
from sympy import *
from sympy import Poly

###############################################
############# Symbols #########################
###############################################

#x = Symbol('x')
#y = Symbol('y')
#z = Symbol('z')

x, y, z = symbols('x y z')

###############################################
############# Functions #######################
###############################################

q = div(eval(poly1eval), eval(poly2eval))