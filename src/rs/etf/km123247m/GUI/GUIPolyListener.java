package rs.etf.km123247m.GUI;

import javafx.scene.control.TextArea;
import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Utils.Divider;
import rs.etf.km123247m.Utils.IPolyListener;
import rs.etf.km123247m.Utils.PolyParser;

/**
 * User: Krca
 * Date: 6/27/13
 * Time: 11:12 PM
 */
public class GUIPolyListener implements IPolyListener {

    TextArea area;
    String dividers;
    String h;
    String q;
    String r;
    Poly sub;
    int dividerPosition;

    public GUIPolyListener(TextArea area, Poly poly1, Poly[] dividers) {
        this.area = area;
        this.h = "(" + poly1.toString().replace("**", "^") + ")";
        this.dividers = "";
        for (Poly poly : dividers) {
            this.dividers += poly.toString().replace("**", "^") + ", ";
        }
        if (this.dividers.length() > 0) {
            this.dividers = this.dividers.substring(0, this.dividers.length() - 2);
        }
        this.dividers = "(" + this.dividers + ")";
    }

    @Override
    public void updateR(Poly newR) {
        r = "(" + newR.toString().replace("**", "^") + ")";
        this.dividerPosition = -1;
    }

    @Override
    public void updateH(Poly newH) {
        h = "(" + newH.toString().replace("**", "^") + ")";
    }

    @Override
    public void updateQ(Poly[] newQ, int dividerPosition) {
        q = "";
        for (Poly poly : newQ) {
            if (poly != null) {
                q += poly.toString() + ", ";
            } else {
                q += "0, ";
            }
        }
        if (q.length() > 0) {
            q = q.substring(0, q.length() - 2);
        }
        q = "(" + q + ")";
        this.dividerPosition = dividerPosition + 1;
    }

    @Override
    public void updateIterationStart(int iteration) {
        area.appendText("\n" + iteration + ". ");
        area.appendText(h + " / " + dividers + " = ");
    }

    @Override
    public void updateIterationEnd(int iteration) {
        sub = Divider.add(PolyParser.parse(h != null ? h.replace("^", "**") : ""), PolyParser.parse(r != null ? r.replace("^", "**") : ""));
        area.appendText((q != null ? q : "( )") + (dividerPosition != -1 ? " [" + dividerPosition + "]" : ""));
        area.appendText("    => (" + (sub != null ? sub.toString().replace("**", "^") : "") + ")\n");
        area.appendText("    H = " + (h != null ? h : ""));
        area.appendText("    R = " + (r != null ? r : "( )"));
        sub = null;
        area.appendText("\n");
    }

    @Override
    public void updateCalculationEnd(Poly[] q, Poly r) {
        String qString = "";
        for (Poly poly : q) {
            if (poly != null) {
                qString += poly.toString() + ", ";
            } else {
                qString += "0, ";
            }
        }
        if (qString.length() > 0) {
            qString = qString.substring(0, qString.length() - 2);
        }
        qString = "(" + qString + ")";
        area.appendText("\nRezultat: Q = " + qString + ", R = (" + r.toString() + ")");
        area.appendText("\n");
    }
}
