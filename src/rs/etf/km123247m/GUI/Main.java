package rs.etf.km123247m.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 10:28 AM
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("layout.fxml"));
        Parent root = (Parent) loader.load();
        Scene scene = new Scene(root, 900, 650);
        stage.setTitle("Deljenje polinoma");
        stage.setScene(scene);
        PolyController controller = loader.getController();
        controller.setStage(stage); // or what you want to do

        stage.show();
    }
}
