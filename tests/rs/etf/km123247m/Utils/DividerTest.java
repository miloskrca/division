package rs.etf.km123247m.Utils;

import org.junit.Before;
import org.junit.Test;
import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Polynomials.PolyElement;
import rs.etf.km123247m.Polynomials.PolyVariable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 8:53 PM
 */
public class DividerTest {
    Poly poly1;
    Poly poly2;
    Poly poly3;
    Poly poly4;
    Poly poly5;

    @Before
    public void setUp() throws Exception {
        PolyElement[] els = new PolyElement[2];
        PolyVariable[] vars11 = new PolyVariable[2];
        PolyVariable[] vars12 = new PolyVariable[2];
        vars11[0] = new PolyVariable('x', 4);
        vars11[1] = new PolyVariable('y', 2);
        els[0] = new PolyElement(vars11);
        vars12[0] = new PolyVariable('y');
        vars12[1] = new PolyVariable('z');
        els[1] = new PolyElement(vars12);
        poly1 = new Poly(els);

        els = new PolyElement[1];
        els[0] = new PolyElement(new PolyVariable[]{
                new PolyVariable('x', 3)
        });
        poly2 = new Poly(els);


//        x + yz + y - z^4 - 4
        poly3 = new Poly(
                new PolyElement[]{
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('x')
                                }
                        ),
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('y'),
                                        new PolyVariable('z')
                                }
                        ),
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('y')
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('z', 4)
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('4')
                                }
                        )
                }
        );

//        y - z^3 - 1
        poly4 = new Poly(
                new PolyElement[]{
                        new PolyElement(
                                new PolyVariable[]{
                                        new PolyVariable('y')
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('z', 3)
                                }
                        ),
                        new PolyElement('-',
                                new PolyVariable[]{
                                        new PolyVariable('1')
                                }
                        )
                }
        );
        poly5 = PolyParser.parse("+x +1");
    }

    @Test
    public void testGetSymbols() throws Exception {
        System.out.println("Testing getSymbols... " + poly1.toString() + " ... ");
        String symbols = poly1.getSymbols();
        assertEquals("x, y, z", symbols);

        symbols = poly1.getElements().get(0).getSymbols();
        assertEquals("x, y", symbols);
    }

    @Test
    public void testDivides() throws Exception {
        System.out.println("Testing Divides... " + poly1.getElements().get(0).toString() + " / " + poly2.getElements().get(0).toString() + " ... ");
        boolean result = poly1.isDividableBy(poly2);
        assertTrue("Ne deli!", result);
    }

    @Test
    public void testDivideFirst() throws Exception {
        System.out.println("Testing DivideFirst... " + poly1.getElements().get(0).toString() + " / " + poly2.getElements().get(0).toString() + " ... ");
        Poly[] result = Divider.divideFirst(poly1, poly2);
        assertTrue("Ne deli!", "+0".equals(result[1].toString()));
        assertEquals("+x*y**2", result[0].toString());
    }
}
