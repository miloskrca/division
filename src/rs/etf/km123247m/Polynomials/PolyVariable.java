package rs.etf.km123247m.Polynomials;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 10:56 AM
 */
public class PolyVariable implements Cloneable {
    private char variable;
    private int power = 1;

    public PolyVariable(char variable) {
        this.variable = variable;
    }

    public PolyVariable(char variable, int power) {
        this.variable = variable;
        this.power = power;
    }

    public char getVariable() {
        return variable;
    }

    public void setVariable(char variable) {
        this.variable = variable;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isDigit() {
        return (this.variable >= '0' && this.variable <= '9');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PolyVariable)) return false;

        PolyVariable that = (PolyVariable) o;

        if (power != that.power) return false;
        if (variable != that.variable) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) variable;
        result = 31 * result + power;
        return result;
    }

    @Override
    public String toString() {
        if (power == 0)
            return "1";
        return variable + (power != 1 ? "**" + power : "");
    }

    public PolyVariable clone() {
        return new PolyVariable(this.getVariable(), this.getPower());
    }
}
