package rs.etf.km123247m.Utils;

import org.junit.Before;
import org.junit.Test;
import rs.etf.km123247m.Polynomials.Poly;

/**
 * User: Krca
 * Date: 6/27/13
 * Time: 8:46 PM
 */
public class IPolyListenerTest {
    Poly poly1;
    Poly poly2;
    Poly poly3;

    private class Listener implements IPolyListener {

        @Override
        public void updateR(Poly r) {
            System.out.println("update R " + r);
        }

        public void updateH(Poly r) {
            System.out.println("update H " + r);
        }

        @Override
        public void updateQ(Poly[] q, int dividerPosition) {
            System.out.println("update Q " + dividerPosition);
            for (Poly poly : q) {
                System.out.println("\t " + poly);
            }
        }

        @Override
        public void updateIterationStart(int iteration) {
            System.out.println("update START iteration " + iteration);
        }

        @Override
        public void updateIterationEnd(int iteration) {
            System.out.println("update END iteration " + iteration);
        }

        @Override
        public void updateCalculationEnd(Poly[] q, Poly r) {
            System.out.println("update END calculation");
        }
    }

    Listener l;

    @Before
    public void setUp() throws Exception {
        poly1 = PolyParser.parse("+x**2*y +x*y**2 +y**2");
        poly2 = PolyParser.parse("+x*y -1");
        poly3 = PolyParser.parse("+y**2 -1");
        l = new Listener();
    }

    @Test
    public void testUpdater() throws Exception {
        System.out.println("Testing Updater... " + poly1 + " / (" + poly2 + ") (" + poly3 + ") ... ");
        poly1.getUpdater().registerQListener(l);
        poly1.getUpdater().registerRListener(l);
        poly1.getUpdater().registerHListener(l);
        poly1.getUpdater().registerIterationStartListener(l);
        poly1.getUpdater().registerIterationEndListener(l);
        Poly[][] result = poly1.divideWith(new Poly[]{poly2, poly3});

//        assertEquals("+x +y", result[0][0].toString());
//        assertEquals("+1", result[0][1].toString());
//        assertEquals("+x +y +1", result[1][0].toString());
    }

}
