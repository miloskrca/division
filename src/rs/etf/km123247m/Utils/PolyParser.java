package rs.etf.km123247m.Utils;

import rs.etf.km123247m.Polynomials.Poly;
import rs.etf.km123247m.Polynomials.PolyElement;
import rs.etf.km123247m.Polynomials.PolyVariable;

import java.util.ArrayList;

/**
 * User: Krca
 * Date: 6/22/13
 * Time: 1:26 PM
 */
public class PolyParser {
    public static Poly parse(String polyString) {
        Poly poly = new Poly();

        if (polyString.length() == 0) {
            return poly;
        }

        if (polyString.contains("Poly")) {
            int comma = polyString.indexOf(",");
            polyString = polyString.substring(5, comma);
        }

        polyString = polyString.replace("[", "")
                .replace("]", "")
                .replace("(", "")
                .replace(")", "")
                .replace(",", "")
                .replace("^", "**")
                .replace("+ ", "+")
                .replace("- ", "-");

        if (polyString.charAt(0) != '+' && polyString.charAt(0) != '-') {
            polyString = "+" + polyString;
        }
        String[] terms = polyString.split(" ");
        char sign;
        for (String term : terms) {
            sign = term.charAt(0);
            term = term.substring(1);
            PolyElement element;
            if (term.length() == 1) {
                element = new PolyElement(sign, new PolyVariable[]{
                        new PolyVariable(term.charAt(0))
                });
                poly.addElement(element);
            } else {
                ArrayList<PolyVariable> variables = new ArrayList<PolyVariable>();
                char var = '-';
                char times = '-';
                int degree = 1;
                boolean endOfTerm = false;
                for (int j = 0; j < term.length(); j++) {
                    char c = term.charAt(j);
                    if (Character.isLetter(c)) {
                        // reset times
                        times = '-';
                        degree = 1;
                        // if next is * and not ** then this is the end of variable definition
                        if (j + 1 < term.length() && term.charAt(j + 1) == '*' && term.charAt(j + 2) != '*') {
                            endOfTerm = true;
                        }
                        if (j == term.length() - 1) {
                            endOfTerm = true;
                        }
                        var = c;
                    } else if (c == '*') {
                        // this does nothing, just reads degree chars
                        degree = 1;
                        if (times == '*') {
                            // this means that we came to a degree
                            times = '^';
                        } else {
                            times = '*';
                        }
                    } else if (Character.isDigit(c)) {
                        if (times == '^') {
                            // the number is a degree
                            degree = Integer.parseInt(String.valueOf(c));
                            times = '-';
                        } else {
                            // this is a number variable
                            endOfTerm = true;
                            var = c;
                            degree = 1;
                        }
                        //if last char of variable than set the end of variable definition
                        if (j == term.length() - 1) {
                            endOfTerm = true;
                        }
                        // if next is * then this is the end of variable definition
                        if (j + 1 < term.length() && term.charAt(j + 1) == '*') {
                            endOfTerm = true;
                        }
                    }

                    // if end of variable
                    if (endOfTerm) {
                        variables.add(new PolyVariable(var, degree));
                        times = 'x';
                        degree = 1;
                        endOfTerm = false;
                    }
                    // if end of element than create new element and add it to poly
                    if (j == term.length() - 1 && variables.size() > 0) {
                        element = new PolyElement(sign, variables.toArray(new PolyVariable[variables.size()]));
                        poly.addElement(element);
                        sign = '+';
                    }
                }
            }
        }

        return poly;
    }
}
