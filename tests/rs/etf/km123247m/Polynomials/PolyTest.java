package rs.etf.km123247m.Polynomials;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 11:58 AM
 */
public class PolyTest {
    Poly poly1;
    Poly poly2;
    Poly poly3;

    @Before
    public void setUp() throws Exception {
        PolyElement[] els = new PolyElement[2];
        PolyVariable[] vars = new PolyVariable[2];
        vars[0] = new PolyVariable('x');
        vars[1] = new PolyVariable('y', 2);
        els[0] = new PolyElement(vars);
        els[1] = new PolyElement(vars);
        poly1 = new Poly(els);

        els = new PolyElement[2];
        vars = new PolyVariable[2];
        vars[0] = new PolyVariable('x');
        vars[1] = new PolyVariable('y', 2);
        els[0] = new PolyElement(vars);
        els[1] = new PolyElement(vars);
        poly2 = new Poly(els);

        els = new PolyElement[2];
        vars = new PolyVariable[2];
        vars[0] = new PolyVariable('x');
        vars[1] = new PolyVariable('y', 2);
        els[0] = new PolyElement(vars);
        els[1] = new PolyElement('-', vars);
        poly3 = new Poly(els);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testEquals() throws Exception {
        System.out.println("Testing Equals... ");
        assertTrue("Equals doesn't work", poly1.equals(poly2));
    }

    @Test
    public void testToString() throws Exception {
        System.out.println("Testing toString... ");
        assertTrue("To String doesn't work", "+x*y**2 +x*y**2".equals(poly1.toString()));
    }

    @Test
    public void testZero() throws Exception {
        System.out.println("Testing Zero... ");
        assertTrue("Zero doesn't work", "+0".equals(Poly.getZero().toString()));
    }

    @Test
    public void testClone() throws Exception {
        System.out.println("Testing Clone... ");
        Poly polyClone = poly1.clone();
        polyClone.getElements().get(0).setSign('-');
        assertTrue("Clone doesn't work! Got a shallow copy!", poly1.getElements().get(0).getSign() != '-');

        polyClone = poly3.clone();
        assertTrue("Clone doesn't work! Sign not copied!", polyClone.getElements().get(1).getSign() == '-');
    }

//    @Test
//    public void testCloneWithoutFirst() throws Exception {
//        System.out.println("Testing CloneWithoutFirst... ");
//        Poly polyClone = poly1.cloneWithoutFirst();
//        assertEquals(polyClone.toString(), "+x*y**2");
//    }

}
