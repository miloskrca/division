from __future__ import division
from sympy import *
from sympy import Poly

###############################################
############# Symbols #########################
###############################################

x, y, z = symbols('x y z')

###############################################
############# Functions #######################
###############################################

#eval(pythonString)
q, r = div(eval(poly1eval), eval(pythonString))