package rs.etf.km123247m.Utils;

import rs.etf.km123247m.Polynomials.*;
import org.python.core.PyString;
import org.python.util.PythonInterpreter;

import java.io.*;

/**
 * User: Krca
 * Date: 6/16/13
 * Time: 8:43 PM
 */
public abstract class Divider {

    static String basePath = null;

    private static String getBasePath() {
    	if(Divider.basePath == null) {
    		Divider.basePath = (new Poly()).getClass().getProtectionDomain().getCodeSource().getLocation().toString();
    		Divider.basePath = Divider.basePath.substring(Divider.basePath.lastIndexOf(':')-1, Divider.basePath.lastIndexOf('/')+1);
    		Divider.basePath = Divider.basePath.replace("%20", " ");
    	}
    	return Divider.basePath;
    }
    
    public static boolean divides(Poly poly1, Poly poly2) {
        if(poly1.getElements().size() == 0) {
            return false;
        }
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = ((PolyElement)poly1.getElements().get(0)).getSymbols();
        String poly2symbols = ((PolyElement)poly2.getElements().get(0)).getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.getElements().get(0).toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.getElements().get(0).toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_divides.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String r = interpreter.get("r").toString();
        String q = interpreter.get("q").toString();

        return r.equals("0") && q.indexOf('/') == -1;
    }

    public static Poly[] divideSingle(Poly poly1, Poly poly2) {
        if(poly1.getElements().size() == 0) {
            return null;
        }
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = poly1.getSymbols();
        String poly2symbols = poly2.getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_divides.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String r = interpreter.get("r").toString();
        String q = interpreter.get("q").toString();

        return new Poly[]{PolyParser.parse(q), PolyParser.parse(r)};
    }

    public static Poly add(Poly poly1, Poly poly2) {
        if(poly1.getElements().size() == 0) {
            return poly2;
        }
        if(poly2.getElements().size() == 0) {
            return poly1;
        }
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = poly1.getSymbols();
        String poly2symbols = poly2.getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_add.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String q = interpreter.get("q").toString();

        return PolyParser.parse(q);
    }

    public static Poly sub(Poly poly1, Poly poly2) {
        if(poly1.getElements().size() == 0) {
            return null;
        }
        if(poly2.getElements().size() == 0) {
            return poly1;
        }
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = poly1.getSymbols();
        String poly2symbols = poly2.getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_sub.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String q = interpreter.get("q").toString();

        return PolyParser.parse(q);
    }

    public static Poly mul(Poly poly1, Poly poly2) {
        if(poly1.getElements().size() == 0 || poly2.getElements().size() == 0) {
            return null;
        }
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = poly1.getSymbols();
        String poly2symbols = poly2.getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_mul.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String q = interpreter.get("q").toString();

        return PolyParser.parse(q);
    }

    public static Poly[] divideFirst(Poly poly1, Poly poly2) {
        PythonInterpreter interpreter = new PythonInterpreter();

        String poly1symbols = ((PolyElement)poly1.getElements().get(0)).getSymbols();
        String poly2symbols = ((PolyElement)poly2.getElements().get(0)).getSymbols();

        String poly1eval = "Poly(poly1 " + (poly1symbols.length() > 0 ? ", " + poly1symbols : ", x") + ", order='lex')";
        String poly2eval = "Poly(poly2 " + (poly2symbols.length() > 0 ? ", " + poly2symbols : ", x") + ", order='lex')";

        interpreter.set("poly1", new PyString(poly1.getElements().get(0).toString()));
        interpreter.set("poly1eval", new PyString(poly1eval));
        interpreter.set("poly2", new PyString(poly2.getElements().get(0).toString()));
        interpreter.set("poly2eval", new PyString(poly2eval));
        File file = new File(Divider.getBasePath() + "python_script_divides.py");
        try {
            interpreter.execfile(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String sr = interpreter.get("r").toString();
        String sq = interpreter.get("q").toString();

        Poly r = PolyParser.parse(sr);
        Poly q = PolyParser.parse(sq);

        return new Poly[]{q, r};
    }
}
