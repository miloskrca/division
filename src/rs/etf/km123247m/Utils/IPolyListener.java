package rs.etf.km123247m.Utils;

import rs.etf.km123247m.Polynomials.Poly;

/**
 * User: Krca
 * Date: 6/27/13
 * Time: 8:34 PM
 */
public interface IPolyListener {
    public void updateR(Poly r);
    public void updateH(Poly h);
    public void updateQ(Poly[] q, int dividerPosition);
    public void updateIterationStart(int iteration);
    public void updateIterationEnd(int iteration);
    public void updateCalculationEnd(Poly[] q, Poly r);
}
